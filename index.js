const axios = require("axios");
const { performance } = require('perf_hooks');


var config = {
    method: 'post',
    url: 'http://localhost:5005/api/exodrvEkey/login',
    headers: { 
      'Authorization': 'Basic YWRtaW46ZyY3QVFocFVUW0BLOC02'
    }
  };
  

( async () => {
    while(true){
        try {
            let time = performance.now();
            let res = await axios(config)
            console.log(res.data)
            console.log(`${(performance.now() - time) / 1000} seconds`);
        } catch (error) {
            console.log(error);
            console.log(`${(performance.now() - time) / 1000} seconds`);
        }
    }
})()